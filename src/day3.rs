use std::cmp;
use std::fs::File;
use std::io::{BufRead, BufReader, Seek};

fn parse(input: &File) -> Vec<isize> {
    BufReader::new(input)
        .lines()
        .map(|line| line.unwrap())
        .map(|line| isize::from_str_radix(&line, 2).unwrap())
        .collect()
}

fn get_num_bits(input: &File) -> usize {
    BufReader::new(input)
        .lines()
        .map(|line| line.unwrap())
        .map(|line| line.len())
        .reduce(cmp::max)
        .unwrap()
}

fn get_bit_mask(input: &File) -> isize {
    let mut mask = 0;

    for i in 0..get_num_bits(input) {
        mask |= 1 << i;
    }

    mask
}

fn count_set_bit(data: &[isize], bit: isize) -> usize {
    data.iter().map(|x| if x & bit != 0 { 1 } else { 0 }).sum()
}

fn calc_gamma(data: &[isize]) -> isize {
    let mut gamma = 0;

    for i in 0..isize::BITS {
        let bit = 1 << i;

        let nr_set = count_set_bit(data, bit);
        if nr_set >= data.len() - nr_set {
            gamma |= bit;
        }
    }

    gamma
}

fn calc_epsilon(data: &[isize], mask: isize) -> isize {
    !calc_gamma(data) & mask
}

pub fn puzzle1(input: &mut File) -> Option<isize> {
    let data = parse(input);

    input.rewind().unwrap();
    let mask = get_bit_mask(input);

    let gamma = calc_gamma(&data);
    let epsilon = calc_epsilon(&data, mask);

    Some(gamma * epsilon)
}

fn fls(val: isize) -> isize {
    1 << (isize::BITS - val.leading_zeros() - 1)
}

fn filter_rating(set: bool, want_set: bool) -> bool {
    if set {
        !want_set
    } else {
        want_set
    }
}

fn find_carbon_rating(data: &[isize], mask: isize) -> isize {
    let mut bit = fls(mask);
    let mut ratings = data.to_vec();

    while ratings.len() > 1 {
        let nr_set = count_set_bit(&ratings, bit);
        let want_set = nr_set >= ratings.len() - nr_set;

        ratings = ratings
            .iter()
            .copied()
            .filter(|x| filter_rating(x & bit != 0, want_set))
            .collect();
        bit >>= 1;
    }

    ratings[0]
}

fn find_oxygen_rating(data: &[isize], mask: isize) -> isize {
    let mut bit = fls(mask);
    let mut ratings = data.to_vec();

    while ratings.len() > 1 {
        let nr_set = count_set_bit(&ratings, bit);
        let want_set = nr_set < ratings.len() - nr_set;

        ratings = ratings
            .iter()
            .copied()
            .filter(|x| filter_rating(x & bit != 0, want_set))
            .collect();
        bit >>= 1;
    }

    ratings[0]
}

pub fn puzzle2(input: &mut File) -> Option<isize> {
    let data = parse(input);

    input.rewind().unwrap();
    let mask = get_bit_mask(input);

    let oxygen = find_oxygen_rating(&data, mask);
    let carbon = find_carbon_rating(&data, mask);

    Some(oxygen * carbon)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::*;

    #[test]
    fn test_puzzle1_example() {
        let mut input = open_testdata("data/day3/example.txt").unwrap();

        assert_eq!(puzzle1(&mut input), Some(198));
    }

    #[test]
    fn test_puzzle1_input() {
        let mut input = open_testdata("data/day3/input.txt").unwrap();

        assert_eq!(puzzle1(&mut input), Some(3959450));
    }

    #[test]
    fn test_puzzle2_example() {
        let mut input = open_testdata("data/day3/example.txt").unwrap();

        assert_eq!(puzzle2(&mut input), Some(230));
    }

    #[test]
    fn test_puzzle2_input() {
        let mut input = open_testdata("data/day3/input.txt").unwrap();

        assert_eq!(puzzle2(&mut input), Some(7440311));
    }
}
