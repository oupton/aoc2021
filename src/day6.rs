use std::fs::File;
use std::io::Read;

fn parse(input: &mut File) -> Option<Vec<usize>> {
    let mut s = String::new();
    input.read_to_string(&mut s).ok()?;

    s.trim()
        .split(',')
        .map(|num| num.parse::<usize>().ok())
        .collect()
}

fn simulate(gens: &mut [isize], days: usize) -> isize {
    for _ in 0..days {
        gens.rotate_left(1);
        gens[6] += gens[8];
    }

    gens.iter().sum()
}

pub fn puzzle1(input: &mut File) -> Option<isize> {
    let data = parse(input)?;
    let mut gens = [0; 9];

    for age in data {
        gens[age] += 1;
    }

    Some(simulate(&mut gens, 80))
}

pub fn puzzle2(input: &mut File) -> Option<isize> {
    let data = parse(input)?;
    let mut gens = [0; 9];

    for age in data {
        gens[age] += 1;
    }

    Some(simulate(&mut gens, 256))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::*;

    #[test]
    fn test_puzzle1_example() {
        let mut input = open_testdata("data/day6/example.txt").unwrap();

        assert_eq!(puzzle1(&mut input), Some(5934));
    }

    #[test]
    fn test_puzzle1_input() {
        let mut input = open_testdata("data/day6/input.txt").unwrap();

        assert_eq!(puzzle1(&mut input), Some(380758));
    }

    #[test]
    fn test_puzzle2_example() {
        let mut input = open_testdata("data/day6/example.txt").unwrap();

        assert_eq!(puzzle2(&mut input), Some(26984457539));
    }

    #[test]
    fn test_puzzle2_input() {
        let mut input = open_testdata("data/day6/input.txt").unwrap();

        assert_eq!(puzzle2(&mut input), Some(1710623015163));
    }
}
