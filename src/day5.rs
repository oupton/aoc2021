use regex::Regex;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Copy, Clone, Eq, PartialEq, Hash)]
struct Point(isize, isize);

impl FromStr for Point {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"^(\d+),(\d+)$").unwrap();
        let caps = re.captures(s).unwrap();

        let x = caps.get(1).unwrap().as_str().parse::<isize>()?;
        let y = caps.get(2).unwrap().as_str().parse::<isize>()?;

        Ok(Point(x, y))
    }
}

impl Point {
    pub fn get_x(&self) -> isize {
        self.0
    }

    pub fn get_y(&self) -> isize {
        self.1
    }
}

struct Line(Point, Point);

impl FromStr for Line {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"^(\d+,\d+)\s+->\s+(\d+,\d+)$").unwrap();
        let caps = re.captures(s).unwrap();

        let start = caps.get(1).unwrap().as_str().parse::<Point>()?;
        let end = caps.get(2).unwrap().as_str().parse::<Point>()?;

        Ok(Line(start, end))
    }
}

impl Line {
    pub fn plot(&self, map: &mut HashMap<Point, usize>) {
        self.iter()
            .for_each(|point| *map.entry(point).or_insert(0) += 1)
    }

    pub fn is_horizontal(&self) -> bool {
        self.0.get_y() == self.1.get_y()
    }

    pub fn is_vertical(&self) -> bool {
        self.0.get_x() == self.1.get_x()
    }

    pub fn iter(&self) -> LineIter {
        LineIter::new(self.0, self.1)
    }
}

struct LineIter {
    start: Point,
    end: Point,
    cur: Option<Point>,
}

impl LineIter {
    pub fn new(start: Point, end: Point) -> LineIter {
        LineIter {
            start,
            end,
            cur: None,
        }
    }
}

impl Iterator for LineIter {
    type Item = Point;

    fn next(&mut self) -> Option<Self::Item> {
        match self.cur {
            Some(p) => {
                if p == self.end {
                    None
                } else {
                    let dx = match p.get_x().cmp(&self.end.get_x()) {
                        Ordering::Less => 1,
                        Ordering::Equal => 0,
                        Ordering::Greater => -1,
                    };

                    let dy = match p.get_y().cmp(&self.end.get_y()) {
                        Ordering::Less => 1,
                        Ordering::Equal => 0,
                        Ordering::Greater => -1,
                    };

                    self.cur = Some(Point(p.get_x() + dx, p.get_y() + dy));
                    self.cur
                }
            }
            None => {
                self.cur = Some(self.start);
                self.cur
            }
        }
    }
}

fn parse(input: &File) -> Vec<Line> {
    BufReader::new(input)
        .lines()
        .map(|line| line.unwrap())
        .map(|line| line.parse::<Line>().unwrap())
        .collect()
}

pub fn puzzle1(input: &File) -> Option<isize> {
    let lines = parse(input);
    let mut map = HashMap::new();

    lines
        .iter()
        .filter(|line| line.is_horizontal() || line.is_vertical())
        .for_each(|line| line.plot(&mut map));

    map.values().filter(|v| **v >= 2).count().try_into().ok()
}

pub fn puzzle2(input: &File) -> Option<isize> {
    let lines = parse(input);
    let mut map = HashMap::new();

    for line in lines {
        line.plot(&mut map);
    }

    map.values().filter(|v| **v >= 2).count().try_into().ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::*;

    #[test]
    fn test_puzzle1_example() {
        let input = open_testdata("data/day5/example.txt").unwrap();

        assert_eq!(puzzle1(&input), Some(5));
    }

    #[test]
    fn test_puzzle1_input() {
        let input = open_testdata("data/day5/input.txt").unwrap();

        assert_eq!(puzzle1(&input), Some(7297));
    }

    #[test]
    fn test_puzzle2_example() {
        let input = open_testdata("data/day5/example.txt").unwrap();

        assert_eq!(puzzle2(&input), Some(12));
    }

    #[test]
    fn test_puzzle2_input() {
        let input = open_testdata("data/day5/input.txt").unwrap();

        assert_eq!(puzzle2(&input), Some(21038));
    }
}
