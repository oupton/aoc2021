use std::collections::HashMap;
use std::fs::File;
use std::io::{Error, Read};
use std::str::FromStr;

struct Board {
    vals: HashMap<isize, (usize, usize)>,
    rows: [u8; 5],
    cols: [u8; 5],
    won: bool,
}

impl Board {
    pub fn mark(&mut self, val: isize) -> bool {
        match self.vals.get(&val) {
            Some((row, col)) => {
                self.rows[*row] += 1;
                self.cols[*col] += 1;

                // Check if we completed a row or column
                self.won |= self.rows[*row] == 5 || self.cols[*col] == 5;
                self.won
            }
            None => false,
        }
    }

    pub fn score(&self, drawn: &[isize]) -> isize {
        let sum: isize = self.vals.keys().sum();
        let sum_marked: isize = drawn.iter().filter(|k| self.vals.contains_key(k)).sum();

        sum - sum_marked
    }

    pub fn won(&self) -> bool {
        self.won
    }
}

impl FromStr for Board {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut board = Board {
            vals: HashMap::new(),
            rows: [0; 5],
            cols: [0; 5],
            won: false,
        };

        for (i, row) in s.split('\n').enumerate() {
            for (j, val) in row.split_whitespace().enumerate() {
                let num = val.parse::<isize>().unwrap();
                board.vals.insert(num, (i, j));
            }
        }

        Ok(board)
    }
}

fn parse(input: &mut File) -> Option<(Vec<isize>, Vec<Board>)> {
    let mut s = String::new();
    input.read_to_string(&mut s).ok()?;

    let split: Vec<&str> = s.split("\n\n").collect();
    let drawn: Vec<isize> = split[0]
        .split(',')
        .map(|line| line.parse::<isize>().unwrap())
        .collect();

    let boards: Vec<Board> = split[1..]
        .iter()
        .map(|s| s.parse::<Board>().unwrap())
        .collect();

    Some((drawn, boards))
}

pub fn puzzle1(input: &mut File) -> Option<isize> {
    let (drawn, mut boards) = parse(input).unwrap();

    for (i, val) in drawn.iter().enumerate() {
        for board in &mut boards {
            if board.mark(*val) {
                let score = board.score(&drawn[..i + 1]);

                return Some(score * val);
            }
        }
    }

    None
}

pub fn puzzle2(input: &mut File) -> Option<isize> {
    let (drawn, mut boards) = parse(input).unwrap();
    let mut final_score = 0;

    for (i, val) in drawn.iter().enumerate() {
        for board in &mut boards {
            if board.mark(*val) {
                let score = board.score(&drawn[..i + 1]);
                final_score = score * val;
            }
        }

        // Stop tracking boards that have already won
        boards.retain(|board| !board.won());
    }

    Some(final_score)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::*;

    #[test]
    fn test_puzzle1_example() {
        let mut input = open_testdata("data/day4/example.txt").unwrap();

        assert_eq!(puzzle1(&mut input), Some(4512));
    }

    #[test]
    fn test_puzzle1_input() {
        let mut input = open_testdata("data/day4/input.txt").unwrap();

        assert_eq!(puzzle1(&mut input), Some(58412));
    }

    #[test]
    fn test_puzzle2_example() {
        let mut input = open_testdata("data/day4/example.txt").unwrap();

        assert_eq!(puzzle2(&mut input), Some(1924));
    }

    #[test]
    fn test_puzzle2_input() {
        let mut input = open_testdata("data/day4/input.txt").unwrap();

        assert_eq!(puzzle2(&mut input), Some(10030));
    }
}
