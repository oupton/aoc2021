// https://adventofcode.com/2021/day/2

use regex::Regex;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn get_vector(s: &str) -> Option<(isize, isize)> {
    let re = Regex::new(r"^(\w+)\s(\d+)$").unwrap();
    let caps = re.captures(s)?;

    let value = caps.get(2)?.as_str().parse::<isize>().ok()?;

    match caps.get(1)?.as_str() {
        "forward" => Some((value, 0)),
        "up" => Some((0, -value)),
        "down" => Some((0, value)),
        _ => None,
    }
}

fn parse(input: &File) -> Vec<(isize, isize)> {
    BufReader::new(input)
        .lines()
        .map(|line| line.unwrap())
        .map(|line| get_vector(&line).unwrap())
        .collect()
}

pub fn puzzle1(input: &File) -> Option<isize> {
    let vectors = parse(input);
    let pos = vectors
        .iter()
        .fold((0, 0), |(ax, ay), (bx, by)| (ax + bx, ay + by));

    Some(pos.0 * pos.1)
}

pub fn puzzle2(input: &File) -> Option<isize> {
    let vectors = parse(input);
    let pos = vectors
        .iter()
        // The first element of the tuple is the slope of the aim. The latter elements
        // are the horizontal position and depth, respectively.
        .fold((0, 0, 0), |(dy, ax, ay), (bx, by)| {
            (dy + by, ax + bx, ay + bx * dy)
        });

    Some(pos.1 * pos.2)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::*;

    #[test]
    fn test_puzzle1_example() {
        let input = open_testdata("data/day2/example.txt").unwrap();

        assert_eq!(puzzle1(&input), Some(150));
    }

    #[test]
    fn test_puzzle1_input() {
        let input = open_testdata("data/day2/input.txt").unwrap();

        assert_eq!(puzzle1(&input), Some(1989014));
    }

    #[test]
    fn test_puzzle2_example() {
        let input = open_testdata("data/day2/example.txt").unwrap();

        assert_eq!(puzzle2(&input), Some(900));
    }

    #[test]
    fn test_puzzle2_input() {
        let input = open_testdata("data/day2/input.txt").unwrap();

        assert_eq!(puzzle2(&input), Some(2006917119));
    }
}
