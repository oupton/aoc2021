// Solutions to the Advent of Code, 2021
//
// https://adventofcode.com/2021

use clap::{App, Arg};
use std::fs::File;
use std::io;
use std::io::Write;
use std::path::Path;
use std::process;

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day8;

fn main() {
    let args = App::new("Advent of Code 2021")
        .author("Oliver Upton <upton.oliver@gmail.com>")
        .arg(
            Arg::with_name("input")
                .short("i")
                .long("input")
                .value_name("FILE")
                .help("file to read test input")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .value_name("FILE")
                .help("file to write test output")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("PUZZLE")
                .help("the puzzle to be solved. For example, Day 1 Puzzle 1 is expressed as 1.1")
                .required(true)
                .index(1),
        )
        .get_matches();

    let mut input = File::open(Path::new(args.value_of("input").unwrap())).unwrap();

    let mut output: Box<dyn Write> = match args.value_of("output") {
        Some(fname) => Box::new(File::create(Path::new(fname)).unwrap()),
        None => Box::new(io::stdout()),
    };

    let answer = match args.value_of("PUZZLE").unwrap() {
        "1.1" => day1::puzzle1(&input),
        "1.2" => day1::puzzle2(&input),
        "2.1" => day2::puzzle1(&input),
        "2.2" => day2::puzzle2(&input),
        "3.1" => day3::puzzle1(&mut input),
        "3.2" => day3::puzzle2(&mut input),
        "4.1" => day4::puzzle1(&mut input),
        "4.2" => day4::puzzle2(&mut input),
        "5.1" => day5::puzzle1(&input),
        "5.2" => day5::puzzle2(&input),
        "6.1" => day6::puzzle1(&mut input),
        "6.2" => day6::puzzle2(&mut input),
        "8.1" => day8::puzzle1(&mut input),
        p => panic!("unrecognized puzzle: {}", p),
    };

    match answer {
        Some(val) => writeln!(output, "{}", val).unwrap(),
        None => {
            eprintln!("couldn't calculate answer");
            process::exit(-1);
        }
    }
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::Result;
    use std::path::PathBuf;

    pub fn open_testdata(sub_path: &str) -> Result<File> {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push(sub_path);

        File::open(path.as_path())
    }
}
