// https://adventofcode.com/2021/day/1

use itertools::izip;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn parse(input: &File) -> Vec<isize> {
    BufReader::new(input)
        .lines()
        .map(|line| line.unwrap())
        .map(|line| line.parse::<isize>().unwrap())
        .collect()
}

fn count_increases(data: &[isize]) -> isize {
    data.iter()
        .zip(data[1..].iter())
        .map(|(x, y)| if x < y { 1 } else { 0 })
        .sum()
}

pub fn puzzle1(input: &File) -> Option<isize> {
    let depths = parse(input);

    Some(count_increases(&depths))
}

fn sliding_sum(data: &[isize]) -> Vec<isize> {
    izip!(data.iter(), data[1..].iter(), data[2..].iter())
        .map(|(x, y, z)| x + y + z)
        .collect()
}

pub fn puzzle2(input: &File) -> Option<isize> {
    let sums = sliding_sum(&parse(input));

    Some(count_increases(&sums))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::*;

    #[test]
    fn test_puzzle1_example() {
        let input = open_testdata("data/day1/example.txt").unwrap();

        assert_eq!(puzzle1(&input), Some(7));
    }

    #[test]
    fn test_puzzle1_input() {
        let input = open_testdata("data/day1/input.txt").unwrap();

        assert_eq!(puzzle1(&input), Some(1139));
    }

    #[test]
    fn test_puzzle2_example() {
        let input = open_testdata("data/day1/example.txt").unwrap();

        assert_eq!(puzzle2(&input), Some(5));
    }

    #[test]
    fn test_puzzle2_input() {
        let input = open_testdata("data/day1/input.txt").unwrap();

        assert_eq!(puzzle2(&input), Some(1103));
    }
}
