use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[allow(clippy::type_complexity)]
fn parse(input: &mut File) -> Option<Vec<(Vec<HashSet<char>>, Vec<HashSet<char>>)>> {
    BufReader::new(input)
        .lines()
        .map(|line| {
            let line = line.ok()?;
            let parts: Vec<&str> = line.split('|').collect();

            match parts.len() {
                2 => {
                    let signals = parts[0]
                        .split(' ')
                        .map(|s| HashSet::from_iter(s.chars()))
                        .collect();
                    let outputs = parts[1]
                        .split(' ')
                        .map(|s| HashSet::from_iter(s.chars()))
                        .collect();

                    Some((signals, outputs))
                }
                _ => None,
            }
        })
        .collect()
}

pub fn unique_signal(signal: &HashSet<char>) -> bool {
    matches!(signal.len(), 2 | 3 | 4 | 7)
}

pub fn puzzle1(input: &mut File) -> Option<isize> {
    let data = parse(input)?;

    let sum: usize = data
        .iter()
        .map(|(_, outputs)| {
            outputs
                .iter()
                .filter(|output| unique_signal(output))
                .count()
        })
        .sum();

    sum.try_into().ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::*;

    #[test]
    fn test_puzzle1_example() {
        let mut input = open_testdata("data/day8/example.txt").unwrap();

        assert_eq!(puzzle1(&mut input), Some(26));
    }

    #[test]
    fn test_puzzle1_input() {
        let mut input = open_testdata("data/day8/input.txt").unwrap();

        assert_eq!(puzzle1(&mut input), Some(310));
    }
}
